<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.modal-nodes.com
 * @since      1.0.0
 *
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/includes
 * @author     Marco Montanari <marco.montanari@gmail.com>
 */
class Wp_Plugin_Listadapter_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-plugin-listadapter',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
