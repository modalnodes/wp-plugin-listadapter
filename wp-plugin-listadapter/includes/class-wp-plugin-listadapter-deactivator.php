<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.modal-nodes.com
 * @since      1.0.0
 *
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/includes
 * @author     Marco Montanari <marco.montanari@gmail.com>
 */
class Wp_Plugin_Listadapter_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
