<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.modal-nodes.com
 * @since      1.0.0
 *
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/includes
 * @author     Marco Montanari <marco.montanari@gmail.com>
 */
class Wp_Plugin_Listadapter_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
