<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://www.modal-nodes.com
 * @since      1.0.0
 *
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Wp_Plugin_Listadapter
 * @subpackage Wp_Plugin_Listadapter/public
 * @author     Marco Montanari <marco.montanari@gmail.com>
 */
class Wp_Plugin_Listadapter_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Plugin_Listadapter_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Plugin_Listadapter_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-plugin-listadapter-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Plugin_Listadapter_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Plugin_Listadapter_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-plugin-listadapter-public.js', array( 'jquery' ), $this->version, false );

	}

	public function register_shortcodes(){
		function list_items($atts){
			//[list_items post_type="profilo" 		item_template="profilo" 		filter_items="active=1"]
			//[list_items post_type="press_release" item_template="press_release" ]
			$atts = shortcode_atts( array(
				"mode"=>"wide",
				"post_type" => "post",
				"posts_per_page" => 10,
				"item_template" => "loop_base",
				"filter_items" => "",
				"filter_fields" => "pub_type"

			), $atts, 'list_items' );

			$ws = $atts["mode"] == "wide";

			$filters = array();
			$f = explode(",",$atts["filter_fields"]);
			foreach ($f as $ff) {
				$filters[$ff] = $_GET[$ff];
			}

			$meta_filters = array();
			$item_filter = explode("&", $atts["filter_items"]);


			foreach ($item_filter as $f) {
				if($f != ""){
					$kvp = explode("=",$f); 
					$k = $kvp[0];
					$v = $kvp[1];
					$meta_filters[] = array("key"=>$k, "value"=>$v);
				}
			}

			$args = array(
		        'post_type' => $atts["post_type"],
		        "posts_per_page" => $atts["posts_per_page"],
		    );

		    if (count($meta_filters) > 0)
		    	$args["meta_query"] = $meta_filters;

		    $string = '';
		    $query = new WP_Query( $args );
		    if( $query->have_posts() ){
		       	$string .= '<ul>';
		        while( $query->have_posts() ){
		            $query->the_post();
		            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
		        	ob_start();
		        	$string .= "<!--".$atts["item_template"]."-->";
		        	get_template_part('includes/modal_nodes',$atts["item_template"]);
		            $string.= ob_get_clean();
		        }
		        $string .= '</ul>';
		    }
		    wp_reset_postdata();
		    return $string;
		}
		add_shortcode( 'list_items', 'list_items');

	}
}
